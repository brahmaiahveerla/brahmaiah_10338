// npm dependencies
import React, { Component } from "react";

// routes for the main App module
import Routes               from "./routes"; 

class App extends Component {
  render() {
    return (     
      <Routes />
    );
  }
}


export default App;
