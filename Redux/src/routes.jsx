// npm dependencies
import React, { Component } from "react";
import { Route, BrowserRouter } from "react-router-dom";

// all components for routing
import Header from "./components/Header/Header";
import EditArticle from "./components/Drives/editDrives";
import AllBulletins from "./components/Bulletins/allBulletens";
import Complaints from "./components/Complaints/complaints";
import Users from "./components/Users/users";
import CreateDrive from "./components/Drives/createDrive";
import LoginForm from './components/Login/loginForm';
import AllDrives from './components/Drives/allDrives'
import Logout from "./components/Logout/logout"
import Role from "./components/Roles/role"
import DriveDetails from './components/Drives/employerDriveDetails'
import Registration from './components/Registration/newRegistration';
import RecruiterDrivePage from './components/Drives/recruiterDrivePage';
import UserDrivePage from './components/Drives/UserDrivePage';
import EmployerDrivePage from './components/Drives/employerDrivePage';
import Footer from './components/Footer/Footer';
import ListHolder from './components/ListHolder/listHolder';
 
/**
 * This is the routes component which .. FILL IN THE BLANKS!
 */
class Routes extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <Route path="/" component={Header} />
          <Route exact path="/dashboard" component={AllDrives} />
          {/* <Route exact path="/article" component={AllArticles} /> */}
          <Route path="/" exact component={LoginForm} />
          <Route exact path="/bulletins" component={AllBulletins} />
          <Route exact path="/article/:id" component={DriveDetails} />
          <Route exact path="/complaints" component={Complaints} />
          <Route exact path="/users" component={Users} />
          <Route exact path="/recruiter/createarticle" component={CreateDrive} />
          <Route exact path="/admin/editarticle/:id" component={EditArticle} />
          <Route exact path="/logout" component={Logout} />
          <Route exact path="/role" component={Role} />
          <Route exact path='/register' component={Registration} />
          <Route exact path='/Rdrives' component={RecruiterDrivePage} />
          <Route exact path='/Edrives' component={EmployerDrivePage} />
          <Route exact path='/Udrives' component={UserDrivePage} />
          <Route exact path='/Edrives' component={Footer} />
          <Route exact path='/listholder' component={ListHolder} />
       
        </React.Fragment>
      </BrowserRouter>
    );
  }
}

export default Routes;
