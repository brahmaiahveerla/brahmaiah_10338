// npm dependencies
import React, { Component } from "react";
import { Link } from 'react-router-dom';

// module dependencies
import LoginForm from '../Login/loginForm'

// styles/assets dependencies
import logo from '../../assets/images/yoalogo.svg';

//styles/assets css file
import '../../assets/css/LayoutStyles/headerStyles.css';

/**
 * Header Component which is common to all other Components
 * (Component with YoA Logo)
 */
class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (   
      <React.Fragment>
        <div className="yoa-admin-header">
          <img className="logo-image" src={logo} alt="" />            
        </div>
      </React.Fragment>
    );
  }
}

export default Header;
