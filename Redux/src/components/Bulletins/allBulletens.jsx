// npm dependencies
import React, { Component } from 'react'
import { Link } from "react-router-dom";


// styles/assets css file 
import '../../assets/css/BaseStyles/baseStyles.css';
import '../../assets/css/LayoutStyles/helperStyles.css';
import '../../assets/css/LayoutStyles/bodyStyles.css';
import '../../assets/css/ModuleStyles/ArticleStyles/searchBarStyle.css';
import '../../assets/css/ModuleStyles/ArticleStyles/leftMenuStyles.css';
import '../../assets/css/ModuleStyles/ArticleStyles/articleCardStyles.css';
import '../../assets/css/BaseStyles/navStyles.css';


import SearchBar from '../SearchBar/searchBar';
import LeftMenu from '../Sidemenu/leftMenu';
import Navbar   from '../Navbar/navbar';
import BulletinCard from '../Cards/bulletinCards';


import {connect} from 'react-redux'

//Action imports
import * as bulletinActions from '../../store/actions/bulletinActionCreator'
import { bindActionCreators } from 'redux';


/**
 * All Bulletins is to display the total number of Bulletins 
 * And These bulletins are being viewed by the Admin
 */

 class AllBulletins extends Component {

  constructor(props) {
    super(props);
  
  }

  componentDidMount(){

        this.props.bulletinActions.fetchBulletins();
    } 

  render() {
    
    return(
      <React.Fragment>
    	<Navbar/>
        <div className="article-container">
          <div className='allarticle-topbar flex-row'>
           
            <SearchBar />
          </div>
          <div className='allarticle-mainbody-holder flex-row'>
            {/* <div className='allarticle-mainbody-leftsection'>
             <LeftMenu /> 
            </div> */}
            <div className='allarticle-mainbody-rightsection'>
              <div className='list-holder'>
              {
        (this.props.bulletinItems) ?
        <div>
          <div className="article-card-holder">	
          {this.props.bulletinItems.map(bulletinDetail => (
            
            <BulletinCard bulletinDetail={bulletinDetail}/>
            
          ))}
          </div>
        </div>:
        <div>Loading</div>
      }
      
              </div>
            </div>
          </div>
          
        </div>
        <div>
    
    </div>
        </React.Fragment>
    )
  }
}



function mapStateToProps(state){

console.log(state);
  return{
  
      bulletinItems:state.bulletinsReducer.bulletins,

  }
}

function mapDispatchToProps(dispatch){

  return{
    bulletinActions: bindActionCreators( bulletinActions,dispatch),
  };
}

export default connect(mapStateToProps,mapDispatchToProps)(AllBulletins);