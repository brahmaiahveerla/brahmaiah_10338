import React,{Component} from 'react';
import { Redirect }             from "react-router-dom";


//Actions for authentication
import {connect} from 'react-redux'

import * as authActions from '../../store/actions/authActionCreator'
import { bindActionCreators } from 'redux';


class LeftMenu extends React.Component{
render(){

  if(localStorage.getItem('jwt-token') != null ){
    
    
    localStorage.removeItem("jwt-token")
    return <Redirect to='/'/>
  }

  return(
      <h1>Successfully Logged out</h1>
  )

}
}

function mapStateToProps(state){
  console.log(state.is_logged_in)
  
  
  return{
      isLoggedIn:state.loginReducer.is_logged_in,
  }
}

function mapDispatchToProps(dispatch){
  return{
      authActions: bindActionCreators(authActions,dispatch),
  };
}

export default connect(mapStateToProps,mapDispatchToProps)(LeftMenu);