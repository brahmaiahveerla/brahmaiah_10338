import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import * as driveActions from '../../store/actions/driveActionCreator';

import { bindActionCreators } from 'redux';

import { connect } from 'react-redux';

import DeleteArticle from '../Modals/deleteArticle.modal';

class RecruiterDriveCards extends Component {

    constructor(props) {
        super(props);
        this.state = {
            is_Drive_Loaded: {},
            myDriveDetails: []
        }
    }
    // handleClick(id, e) {
    //     if (window.confirm("Do you want to delete article")) {
    //         this.props.allActions.deleteArticle(id);
    //         setTimeout(() => {
    //             window.location.reload()
    //         }, 100)
    //     }
    // }
    render() {

        return (
            <div>
                {
                    (this.props.rec_DriveDetails) ?
                        (this.props.is_Rdrive_Loaded) ?
                            <div>
                                {
                                    (this.props.rec_DriveDetails.response) ?
                                        <div className="container">
                                            <div className="row">
                                                {this.props.rec_DriveDetails.response.map((driveDetails) => (
                                                    <div className="article-card ">
                                                        <div className='flex-row article-card-topheader'>
                                                            <div className='img-holder'>
                                                                <img className='article-img' src={driveDetails.image} />
                                                            </div>
                                                            <div className='flex-column article-txt-data'>
                                                                <p className='article-header-txt'>{driveDetails.heading}</p>
                                                                <p className='writer'>Venue : {driveDetails.where}</p>
                                                                <p className='article-brief'>Apply : {driveDetails.url}</p>
                                                                <p className='article-header-txt'>Last Day:{driveDetails.register_before}</p>
                                                                <div className='btn-holder'>
                                                                    <Link to={`article/${driveDetails.driveId}`}><button className="edit-btn site-btn">Read</button></Link>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))}
                                            </div>
                                        </div> :
                                        <div>
                                            No Recruiter Drives
                                        </div>
                                }
                            </div> :
                            <div>Loading..</div> :
                        <div>Loading..</div>
                }
            </div>
        )
    }
}
function mapStateToProps(state) {
   
    console.log(state);
    return {
        is_Rdrive_Loaded: state.drivesReducer.is_R_Loaded,
        rec_DriveDetails: state.drivesReducer.recruiterDrives,
        userData: state.loginReducer.user_details
    }
}
function mapDispatchToProps(dispatch) {
    return {
        allActions: bindActionCreators(driveActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(RecruiterDriveCards);