import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import * as driveActions from '../../store/actions/driveActionCreator';

import { bindActionCreators } from 'redux';

import { connect } from 'react-redux'; 

class EmployerDriveCards extends Component {

    constructor(props) {
        super(props);
        this.state = {
            is_Drive_Loaded: {},
            myDriveDetails: []
        }
    }
    render() {
       
        return (
            <div>
                {
                    (this.props.rec_DriveDetails) ?
                        
                            <div>
                                {
                                    (this.props.rec_DriveDetails.length > 0) ?
                                        <div className="container">
                                            <div className="row">
                                                {this.props.rec_DriveDetails.map((driveDetails) => (
                                                    <div className="article-card ">
                                                        <div className='flex-row article-card-topheader drivecard'>
                                                            <div className='img-holder'>
                                                                <img className='article-img' src={driveDetails.image} />
                                                            </div>
                                                            <div className='flex-column article-txt-data'>
                                                                <p className='article-header-txt'>{driveDetails.heading}</p>
                                                                <p className='writer'>Venue : {driveDetails.where}</p>  
                                                                <p className='article-brief'>Apply : {driveDetails.url}</p>
                                                                <p className='article-header-txt'>Last Day:{driveDetails.register_before}</p>
                                                                <div className='btn-holder'>
                                                                    <Link to={`article/${driveDetails.driveId}`}><button className="btn btn-primary" onClick = {this.read}>Read</button></Link>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))}
                                            </div>
                                        </div> :
                                        <div>
                                            No Employer Drives
                                        </div>
                                }
                            </div> :  <div>Loading..</div>                      
                        
                }
            </div>
        )
    }
}
function mapStateToProps(state) {
    
    console.log(state);
    return {
        is_Edrive_Loaded: state.drivesReducer.is_E_Loaded,
        rec_DriveDetails: state.drivesReducer.employerDrives,
        userData: state.loginReducer.user_details
    }
}
 
export default connect(mapStateToProps )(EmployerDriveCards);