import React, { Component } from 'react';

import * as driveActions from '../../store/actions/driveActionCreator';

export default class EmployerDriveDetailsCards extends Component {

    constructor(props) {
        super(props);
        this.state = {
            driveDetails: [],
        }
    }

    handleModal(id){
        localStorage.setItem('feedbackid',id)
    }
    render() {
        const { Details } = this.props
        const feedbackid = localStorage.getItem('feedbackid')
        const completedInterviews = Details.completedInterviews;
        const scheduledInterviews = Details.scheduledInterviews;

        return (
            <div className="container flex-row">
                {
                    (scheduledInterviews) ?

                        <div className="col-sm-6">
                            {
                                <div  >
                                    <center>  <h5 className="empdriveheading" >Drive scheduled Candidates</h5></center>
                                    {(scheduledInterviews.length) ?
                                        <div>
                                            {scheduledInterviews.map((driveDetails) => (

                                                <div className="article-card  Candidates ">
                                                    <div className='flex-row article-card-topheader '>
                                                        <div className='img-holder'>
                                                            <img className='article-img' src={driveDetails.candidate.image} />
                                                        </div>
                                                        <div className='flex-column article-txt-data candidatedata'>
                                                            <p className='article-header-txt'><b> Name : </b> {driveDetails.candidate.firstName} {driveDetails.candidate.lastName}</p>
                                                            <p className='article-header-txt'><b> Id :</b> {driveDetails.candidate.userId}</p>
                                                            <p className='article-header-txt'><b> Email id : </b>{driveDetails.candidate.email} </p>
                                                            <p className='article-header-txt'><b> phone No :</b> {driveDetails.candidate.phoneNo}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                        : <div><center> <h5 className="empdriveheading" > No candidates</h5></center></div>}

                                </div>

                            }
                        </div> : <div>loading...</div>

                }

                {
                    (completedInterviews) ?
                        <div className="col-sm-6">
                            {
                                <div >
                                    <center>   <h5 className="empdriveheading" > Drive Completed Candidates</h5>  </center>
                                    {(completedInterviews.length) ?
                                        <div>
                                            {completedInterviews.map((completdriveDetails) => (
                                                <div className="article-card  Candidates ">
                                                    <div className='flex-row article-card-topheader '>
                                                        <div className='img-holder'>
                                                            <img className='article-img' src={completdriveDetails.candidate.image} />
                                                        </div>
                                                        <div className='flex-column article-txt-data candidatedata'>
                                                            <p className='article-header-txt'><b> Name : </b>{completdriveDetails.candidate.firstName} {completdriveDetails.candidate.lastName} </p>
                                                            <p className='article-header-txt'><b> Id : </b>{completdriveDetails.candidate.userId} </p>
                                                            <p className='article-header-txt'><b> Email id : </b>{completdriveDetails.candidate.email} </p>
                                                            <p className='article-header-txt'><b> phone No: </b>{completdriveDetails.candidate.phoneNo} </p>

                                                        </div>

                                                    </div>
                                                    <div className='flex-row article-card-topheader '>
                                                        <button data-target=".myModal" data-toggle="modal" onClick={this.handleModal.bind(this,completdriveDetails.candidate.userId)} >Feedback</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <p> <b>Interviewers FeedBack: </b> </p>  &nbsp; {(completdriveDetails.feedBack.is_recommended) ? <b1> Recommended </b1> : <b1> Not Recommended</b1>}

                                                    </div>
                                                    <div class="modal fade myModal" id="myModal" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-head">
                                                                    <center> <h5 className="empdriveheading" >Feed Back</h5></center>
                                                                </div>
                                                                <div class="modal-body">
                                                                {console.log(completedInterviews.candidate)}
                                                                    {completedInterviews.map((completdriveDetails) => (
                                                                   
                                                                        <div className="flex-row">
                                                                            <div>
                                                                                <img className='article-img' src={completdriveDetails.candidate.image} />
                                                                            </div>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                            <div>
                                                                                <p className='header-txt'><b> Name : </b>{completdriveDetails.candidate.firstName} {completdriveDetails.candidate.lastName} </p>
                                                                                <p className='header-txt'><b> Email id : </b>{completdriveDetails.candidate.email} </p>
                                                                                <p className='header-txt'><b> phone No: </b>{completdriveDetails.candidate.phoneNo} </p>
                                                                            </div>
                                                                        </div>
                                                                     
                                                                    ))}
                                                                    <hr></hr>
                                                                    <center> <h5 className="empdriveheading" >Interviewers FeedBack</h5></center>
                                                                    {completedInterviews.map((completdriveDetails) => (
                                                                        <div>
                                                                            <p><b> Comments: </b>{completdriveDetails.feedBack.comments} </p>
                                                                            <p><b>Recommended : </b>{
                                                                                (completdriveDetails.feedBack.is_recommended) ? <i>  Recommended </i> : <i> NotRecommended</i>
                                                                            } </p>
                                                                            {
                                                                                completdriveDetails.feedBack.drill_down_feedback.map((feedBack) => (
                                                                                    <div>
                                                                                        <p><b> Comment : </b> {feedBack.comment}</p>
                                                                                        <p><b>FeedBack : </b> {feedBack.feedBack}</p>
                                                                                        <p><b> Rating : </b> {feedBack.rating}</p>
                                                                                        <p><b> Skill_category : </b> {feedBack.skill_category}</p>
                                                                                        <hr />
                                                                                        <center> <h5 className="empdriveheading" >Recruiter FeedBack</h5></center>
                                                                                        <div className="container flex-row">
                                                                                            <div>
                                                                                                <p><b>Skill : </b> {feedBack.skill}</p>
                                                                                                <p><b>Cutoff_rating : </b> {feedBack.cutoff_rating}</p>
                                                                                                <p><b>Max Rating: </b> {feedBack.max_rating}</p>
                                                                                            </div >&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                        <div>
                                                                                                <video width="250" controls>
                                                                                                    <source src="https://www.w3schools.com/css/mov_bbb.mp4" type="video/mp4" />
                                                                                                </video>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                ))
                                                                            }
                                                                            {/* <p> {completdriveDetails.feedBack.interview_video}</p> */}
                                                                        </div>
                                                                    ))}
                                                                </div>

                                                                <button type="button" class="btn btn-secondary modelclose" data-dismiss="modal">Close</button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            ))}
                                        </div>
                                        : <div><center> <h5 className="empdriveheading"  > No candidates</h5></center></div>}
                                </div>

                            }
                        </div> : <div>No Candidates</div>

                }
            </div>

        )
    }
}

