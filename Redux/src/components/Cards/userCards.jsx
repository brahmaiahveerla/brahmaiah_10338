import React, { Component } from 'react';

import '../../assets/css/ModuleStyles/UserStyles/userCardStyles.css';

class UserCard extends Component{
	render(){
		const { userDetail } = this.props
		return(
			<React.Fragment>
				<div className="article-card-holder">	
					<div className="article-card ">
						<div className='flex-row article-card-topheader'>
							<div className='img-holder'>
								<img className='article-img' src={userDetail.img}/>
							</div>
							<div className='article-txt-data flex-row '>
								<p className='writer'>{userDetail.name}</p>
								<button className="delete-btn-user site-btn">Delete</button>	
							</div>
						</div>
					</div>
				</div>
			</React.Fragment>
    	)	
	}
}

export default UserCard;