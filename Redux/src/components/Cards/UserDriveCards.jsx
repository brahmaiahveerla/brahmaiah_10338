import React, { Component } from 'react';

import { Link } from 'react-router-dom';

import * as allActions from '../../store/actions/driveActionCreator';

import { bindActionCreators } from 'redux';

import { connect } from 'react-redux';

import DeleteArticle from '../Modals/deleteArticle.modal';

import '../../assets/css/drives.css';



class UserDriveCards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            myDriveDetails: {}
        }
    }

    render() {

        return (
            <div>
                {
                    (this.props.myDriveDetails) ?
                        (this.props.myDriveDetails.regiteredDrives) ?
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-6">                                       
                                        <div className="article-card-holder changes">
                                        <div className="col-md-6">
                                            <p className="container-styles">Registered Drives {this.props.myDriveDetails.regiteredDrives.length} </p>
                                        </div>

                                            {this.props.myDriveDetails.regiteredDrives.map((driveDetails) => (
                                                <div className="article-card ">
                                                    <div className='flex-row article-card-topheader'>
                                                        <div className='img-holder'>
                                                            <img className='article-img' src={driveDetails.image} />
                                                        </div>
                                                        <div className='flex-column article-txt-data'>
                                                            <p className='article-header-txt'>{driveDetails.heading}</p>
                                                            <p className='writer'>Venue : {driveDetails.where}</p>
                                                            <p className='article-brief'>Apply : {driveDetails.url}</p>
                                                            <p className='article-header-txt'>Last Day:{driveDetails.register_before}</p>
                                                            <div className='btn-holder'>
                                                                <Link to={`article/${driveDetails.driveId}`}><button className="edit-btn site-btn">Read</button></Link>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                    <div className="col-md-6">                                      
                                        <div className="article-card-holder changes">
                                        <div className="col-md-6">
                                            <p className="container-styles">Suggested Drives {this.props.myDriveDetails.suggestedDrives.length} </p>
                                        </div>

                                            {this.props.myDriveDetails.suggestedDrives.map((driveDetails) => (
                                                <div className="article-card ">
                                                    <div className='flex-row article-card-topheader'>
                                                        <div className='img-holder'>
                                                            <img className='article-img' src={driveDetails.image} />
                                                        </div>
                                                        <div className='flex-column article-txt-data'>
                                                            <p className='article-header-txt'>{driveDetails.heading}</p>
                                                            <p className='writer'>Venue : {driveDetails.where}</p>
                                                            <p className='article-brief'>Apply : {driveDetails.url}</p>
                                                            <p className='article-header-txt'>Last Day:{driveDetails.register_before}</p>
                                                            <div className='btn-holder'>
                                                                <Link to={`article/${driveDetails.driveId}`}><button className="edit-btn site-btn">Read</button></Link>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            </div> :
                            <div>Loading...</div> :

                        <div>Loading...</div>
                }
            </div>
        )
    }
}
function mapStateToProps(state) {
 
    console.log(state);
    console.log("User Drive Cards");
    return {
        is_Drive_Loaded: state.drivesReducer.isLoaded,
        myDriveDetails: state.drivesReducer.myDrives,
        userData: state.loginReducer.user_details
    }
}
function mapDispatchToProps(dispatch) {
    return {
        allActions: bindActionCreators(allActions, dispatch),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(UserDriveCards);