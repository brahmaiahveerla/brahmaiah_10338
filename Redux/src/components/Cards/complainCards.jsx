import React, { Component } from 'react';


class BulletinCard extends Component{
	render(){
		const { complainDetail } = this.props; 
		return(
			<React.Fragment>
				<div className="article-card-holder">	
					<div className="article-card ">
						<div className='flex-row article-card-topheader'>
							<div className='img-holder'>
								<img className='article-img' src={complainDetail.img}/>
							</div>
							<div className='flex-column article-txt-data'>
								<p className='article-header-txt'>{complainDetail.title}</p>
								<p className='writer'>{complainDetail.name}</p>
								<p className='posting-date'>{complainDetail.time.slice(0,10)}</p>
								<p className='article-brief'>{complainDetail.discription}</p>
								<div className='btn-holder'>
									<button className="delete-btn site-btn">Resolve</button>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</React.Fragment>
    	)	
	}
}

export default BulletinCard;