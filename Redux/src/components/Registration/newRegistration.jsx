import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import * as registrationActions from '../../store/actions/newRegistrationCreator';
import '../../assets/css/LayoutStyles/createDriveStyles.css';
import * as NewRegistrationActions from '../../store/actions/driveRegistrationActionCreators';

class NewRegistration extends Component {
  constructor() {
    super();
    this.state = {
      fields: {},
      errors: {},
    }

    this.handleChange = this.handleChange.bind(this);
    this.submituserRegistrationForm = this.submituserRegistrationForm.bind(this);

  };

  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });

  }

  submituserRegistrationForm(e) {
    e.preventDefault();
    if (this.validateForm()) {
      let fields = {};
      fields["first_name"] = "";
      fields["last_name"] = "";
      fields["email"] = "";
      fields["password"] = "";
      fields["phone_no"] = "";

      this.setState({
        fields: fields
      });

      this.props.NewRegistrationActions.newUserRegistration({
        firstName: this.state.fields["first_name"],
        lastName: this.state.fields["last_name"],
        email: this.state.fields["email"],
        password: this.state.fields["password"],
        phone_no: this.state.fields["phone_no"],
        skill_tags: this.state.fields["skill_tags"]
      })

    }
  }

  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    if (!fields["first_name"] || fields["first_name"]) {
      formIsValid = false;
      let fName = fields["first_name"];
      if (fName) {
        if (fName.length > 4) {
          formIsValid = true;
        }
        else {
          errors["first_name"] = "*Please enter your First name atleast 4 Characters.";
        }
      }
      else {
        errors["first_name"] = "*Please enter your  First name.";
      }
    }

    if (!fields["last_name"] || fields["last_name"]) {
      formIsValid = false;
      let lName = fields["last_name"];
      if (lName) {
        if (lName.length > 4) {
          formIsValid = true;
        }
        else {
          errors["last_name"] = "*Please enter your Last name atleast 4 Characters.";
        }
      }
      else {
        errors["last_name"] = "*Please enter your  Last name .";
      }
    }

    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "*Please enter your email-ID.";
    }

    if (typeof fields["email"] !== "undefined") {
      //regular expression for email validation
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if (!pattern.test(fields["email"])) {
        formIsValid = false;
        errors["email"] = "*Please enter valid email-ID.";
      }
    }

    if (!fields["phone_no"]) {
      formIsValid = false;
      errors["phone_no"] = "*Please enter your mobile no.";
    }

    if (typeof fields["phone_no"] !== "undefined") {
      if (!fields["phone_no"].match(/^[0-9]{10}$/)) {
        formIsValid = false;
        errors["phone_no"] = "*Please enter valid mobile no.";
      }
    }

    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "*Please enter your password.";
    }

    if (typeof fields["password"] !== "undefined") {
      if (!fields["password"].match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {
        formIsValid = false;
        errors["password"] = "*Please enter secure and strong password.";
      }
    }

    this.setState({
      errors: errors
    });
    return formIsValid;


  }

  render() {
    if (this.props.userRole) {
      if (this.props.status) {
        if (window.confirm("You are successfully Registered"))
          return <Redirect to="./" />
      }
    }
    return (
      <div className="main-registration-container">
        <div className="register">

          <h3 className="new-registration">New Registration</h3>
          <form name="userRegistrationForm" >

            <label>First Name</label>
            <input type="text" name="first_name" placeholder="Enter your first name" value={this.state.fields.first_name} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.first_name}</div>

            <label>Last Name</label>
            <input type="text" name="last_name" placeholder="Enter your last name" value={this.state.fields.last_name} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.last_name}</div>

            <label>Email</label>
            <input type="email" name="email" placeholder="Enter your Email Id" value={this.state.fields.email} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.email}</div>

            <label>Password</label>
            <input type="password" name="password" placeholder="Enter your password" value={this.state.fields.password} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.password}</div>

            <label>Date of Birth</label>
            <input type="date" name="dat_of_birth" value={this.state.fields.dat_of_birth} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.dat_of_birth}</div>

            <label>Last date to Apply:</label>
            <input type="date" name="before" value={this.state.fields.before} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.before}</div>

            <label>Location</label>
            <input type="text" name="location" placeholder="Enter your location" value={this.state.fields.location} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.location}</div>

            <label>Skills</label>
            <input type="text" name="skill_tags" placeholder="Enter your skills" value={this.state.fields.skill_tags} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.skill_tags}</div>

            <label>Languages</label>
            <input type="text" name="languages_tags" placeholder="Enter your languages" value={this.state.fields.languages_tags} onChange={this.handleChange} rows={5} coloumns={100}></input>
            <div className="errorMsg">{this.state.errors.languages_tags}</div>

            <label>Phone Number</label>
            <input type="text" name="phone_no" placeholder="Enter your phone number" value={this.state.fields.phone_no} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.phone_no}</div>

            <label>Upload Image</label>
            <input type="file" name="image" value={this.state.fields.image} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.image}</div>

            <input type="button" className="button" value="Register" onClick={this.submituserRegistrationForm} />
          </form>
        </div>
      </div>

    );
  }
}

function mapStateToProps(state) {
  return {
    userRole: state.registrationReducer.registrationResponse,
    status: state.registrationReducer.registration_status
  }
}

function mapDispatchToProps(dispatch) {
  return {
    NewRegistrationActions: bindActionCreators(registrationActions, dispatch),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(NewRegistration);