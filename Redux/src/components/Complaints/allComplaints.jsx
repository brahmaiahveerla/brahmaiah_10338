// npm dependencies
import React, { Component } from 'react';

// styles/assets css file 
import '../../assets/css/allarticles.css'
import complaintservice from './complaintservice';

/**
 * All Complaints is used to display the total number of complaints 
 * And These complaints are being managed by the Admin
 */

export default class AllComplaints extends Component {

  constructor(props) {
    super(props);

    this.state = {
      complaints: complaintservice
    }
  }

  handleDeleteComplaint(i) {
    var confirmation = window.confirm('Are you sure you wish to delete this item?');
    if (confirmation) {
      if (confirmation == true) {
        let complaints = [...this.state.complaints]
        complaints.splice(i, 1)
        this.setState({
          complaints: complaints
        })
      }
      else if (confirmation == false) {
        document.write("User wants to false continue!");
      }
    }
    console.log(i);
  }

  render() {
    let complaint_List = this.state.complaints.map(complaint => {
      return (
        <div>
          <div className="row articlerow">
            <div className="col-sm-2 articlerowimg ">
              <img src={complaint.img} className=" imagecorner" />
            </div>
            <div className="col-sm-10 articlerowtext">
              <p className="articles-heading-text" id="top">{complaint.title} </p>
              <p className="writer-name" id="size">{complaint.name} </p>
              <p className="write-time" id="size">{complaint.time} </p>
            </div>
            <div className="col-sm-12 articlerowtext" id="top">
              <p>{complaint.text} </p>
            </div>
            <div className="col-sm-12 articlerowtext" id="bottom">
              <button type="button" className="btn btn-primary Delete"
                onClick={index => this.handleDeleteComplaint(index)}>Resolve</button>
            </div>
          </div>
        </div>
      );
    });


    return (
      <div className="col-sm-8 Articlearea">
        {complaint_List}
      </div>
    )
  }
}