// npm dependencies
import React, { Component } from 'react'
import { Link } from "react-router-dom";
import LeftMenu from '../Sidemenu/leftMenu';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';


//Action imports
import * as driveActions from '../../store/actions/driveActionCreator';
import { bindActionCreators } from 'redux';
import '../../assets/css/LayoutStyles/createDriveStyles.css';
/**
 * create Article Component is used by Admin to create new articles
 */
class ListHolder extends React.Component {
  constructor() {
    super();
    this.state = {
      fields: {},
      errors: {}
    }

    this.handleChange = this.handleChange.bind(this);
    this.submituserRegistrationForm = this.submituserRegistrationForm.bind(this);

  };

  handleChange(e) {
    let fields = this.state.fields;
    fields[e.target.name] = e.target.value;
    this.setState({
      fields
    });

  }

  submituserRegistrationForm(e) {
 
    e.preventDefault();
    if (this.validateForm()) {
      let fields = {};
      fields["heading"] = "";
      fields["subHeading"] = "";
      fields["description"] = "";
      fields["city"] = "";
      fields["exactly"] = "";
      fields["when"] = "";
      fields["where"]="";
      fields["url"] = "";
      fields["image"] = "";
      fields["eligibility"] = "";
      fields["more"] = "";
      fields["phone"] = "";

      this.setState({
        fields: fields
      });

      this.props.create_my_Drive.createDrive({
       field:{
        heading : this.state.fields["heading"],
        sub_heading: this.state.fields["subHeading"],
        description: this.state.fields["description"],
        city: this.state.fields["city"],
        exactly: this.state.fields["exactly"], 
        when: this.state.fields["when"], 
        url: this.state.fields["url"], 
        image: this.state.fields["image"], 
        eligibility: this.state.fields["eligibility"], 
        more_information : this.state.fields["more"],
        phone : this.state.fields["phone"],
        where: this.state.fields["where"]
       },
        id: this.props.userData.response.userId
      })

    }
  }

  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    // if (!fields["heading"] || fields["heading"]) {
    //   formIsValid = false;
    //   var head = fields["heading"];
    //   if (head) {
    //     if (head.length < 4 || head.length > 30) {
    //       errors["heading"] = "Heading should not be less than 3 Or more than 20 characters."
    //     }
    //     else {
    //       formIsValid = true;
    //     }
    //   }
    //   else {
    //     errors["heading"] = "*Please Enter the Heading.";
    //   }
    // }

    // if (!fields["subHeading"] || fields["subHeading"]) {
    //   formIsValid = false;
    //   var subHead = fields["subHeading"];
    //   if (subHead) {
    //     if (subHead.length < 4 || subHead.length > 30) {
    //       errors["subHeading"] = "subHeading should not be less than 3 Or more than 20 characters."
    //     }
    //     else {
    //       formIsValid = true;
    //     }
    //   }
    //   else {
    //     errors["subHeading"] = "*Please enter sub Heading.";
    //   }
    // }

    // if (!fields["description"] || fields["description"]) {
    //   formIsValid = false;
    //   var description = fields["description"];
    //   if (description) {
    //     if (description.length < 40 || description.length > 200) {
    //       errors["description"] = "description should not be less than 40 Or more than 200 characters."
    //     }
    //     else {
    //       formIsValid = true;
    //     }
    //   }
    //   else {
    //     errors["description"] = "*Please enter Description.";
    //   }
    // }

    // if (!fields["city"] || fields["city"]) {
    //   formIsValid = false;
    //   var city = fields["city"];
    //   if (city) {
    //     if (city.length < 3 || city.length > 10) {
    //       errors["city"] = "city should not be less than 3 Or more than 10 characters."
    //     }
    //     else {
    //       formIsValid = true;
    //     }
    //   }
    //   else {
    //     errors["city"] = "*Please enter the City.";
    //   }
    // }

    // if (!fields["exactly"] || fields["exactly"]) {
    //   formIsValid = false;
    //   var exactly = fields["exactly"];
    //   if (exactly) {
    //     if (exactly.length < 3 || exactly.length > 10) {
    //       errors["exactly"] = "exactly should not be less than 3 Or more than 10 characters."
    //     }
    //     else {
    //       formIsValid = true;
    //     }
    //   }
    //   else {
    //     errors["exactly"] = "*Please enter the exactly Details.";
    //   }
    // }

    if (fields["when"]) {
      let date = fields["when"];
      let datearr = date.split('-');
      let selectedFormat = new Date(datearr[0], datearr[1] - 1, datearr[2]);
      let today = new Date();
      let n = today.getTime();
      if (selectedFormat.getTime() > n) {
        formIsValid = true;
      }
      else {
        errors["when"] = "Your Drive should not be today or past day";
      }
    } else {
      formIsValid = false;
      errors["when"] = "please select the date of Drive";
    }

    // if (fields["time"]) {
    //   var time = fields["time"];
    //   console.log(time.split(':'));
    //   var timeArr = time.split(':');
    //   if (timeArr) {
    //     if (timeArr[0] > 8 && timeArr < 18) {
    //       formIsValid = true;
    //     }
    //     else {
    //       formIsValid = false;
    //       errors["time"] = "exactly time should be in between 8 AM and 6 PM."
    //     }
    //   }
    // } else {
    //   formIsValid = false;
    //   errors["time"] = "*Please enter the exactly timings.";
    // }

    // if (fields["before"]) {
    //   let before = fields["before"];
    //   let beforearr = before.split('-');
    //   let selectedBeforeFormat = new Date(beforearr[0], beforearr[1] - 1, beforearr[2]);
    //   let today = new Date();
    //   let n = today.getTime();
    //   let sel = selectedBeforeFormat.getTime();
    //   if (sel >= n) {
    //     formIsValid = true;
    //   }
    //   else {
    //     formIsValid = false;
    //     errors["before"] = "Your Drive registration day should not be past day.";
    //   }
    // } else {
    //   formIsValid = false;
    //   errors["before"] = "please select the last date to Apply for the Drive.";
    // }

    // if (!fields["url"]) {
    //   formIsValid = false;
    //   var url = fields["url"];
    //   if (url) {
    //     if (url.length < 8) {
    //       errors["url"] = "Heading should not be less than 8 characters."
    //     }
    //     else {
    //       formIsValid = true;
    //     }
    //   }
    //   else {
    //     errors["url"] = "*Please Enter the URL.";
    //   }
    // }

    // if (fields["image"]) {
    //   formIsValid = true;
    //   let x = fields["image"];
    //   var str = x.replace(/.*[\/\\]/, '');
    //   var result = "";
    //   var resarr = str.split(".");
    //   for (var i = 0; i < resarr.length; i++) {
    //     if (i == resarr.length - 1) {
    //       result = result + "." + resarr[i];
    //     }
    //     else {
    //       result = result + resarr[i];
    //     }
    //   }
    //   fields["image"] = result;
    // } else {
    //   formIsValid = false;
    //   errors["image"] = "*please select image."
    // }

    // if (!fields["eligibility"] || fields["eligibility"]) {
    //   formIsValid = false;
    //   var eligibility = fields["eligibility"];
    //   if (eligibility) {
    //     if (eligibility.length < 40 || eligibility.length > 200) {
    //       errors["eligibility"] = "eligibility criteria should not be less than 40 Or more than 200 characters."
    //     }
    //     else {
    //       formIsValid = true;
    //     }
    //   }
    //   else {
    //     errors["eligibility"] = "*Please enter eligibility.";
    //   }
    // }

    // if (!fields["more"]) {
    //   formIsValid = false;
    //   var more = fields["more"];
    //   if (more) {
    //     if (more.length < 40 || more.length > 200) {
    //       errors["more"] = "more information should not be less than 40 Or more than 200 characters."
    //     }
    //     else {
    //       formIsValid = true;
    //     }
    //   }
    //   else {
    //     errors["more"] = "*Please enter more.";
    //   }
    // }

    // if (typeof fields["phone"] !== "undefined") {
    //   if (!fields["phone"].match(/^[0-9]{10}$/)) {
    //     formIsValid = false;
    //     errors["phone"] = "*Please enter valid mobile no.";
    //   }
    // }

    this.setState({
      errors: errors
    });
    return formIsValid;
  }

  render() {
    return (
      <div className="main-registration-container">
        <div className="register">

          <h3 className="drive">Create Drive</h3>
          <form method="post" name="userRegistrationForm" >

            <label>Heading</label>
            <input type="text" name="heading" value={this.state.fields.heading} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.heading}</div>

            <label>Sub Heading</label>
            <input type="text" name="subHeading" value={this.state.fields.subHeading} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.subHeading}</div>

            <label>Description</label>
            <textarea className="description_area" type="text" name="description" value={this.state.fields.description} onChange={this.handleChange} rows={5} coloumns={20}></textarea>
            <div className="errorMsg">{this.state.errors.description}</div>

            <label>City</label>
            <input type="text" name="city" value={this.state.fields.city} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.city}</div>
            
            <label>Venue</label>
            <input type="text" name="where" value={this.state.fields.where} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.where}</div>

            <label>Exactly</label>
            <input type="text" name="exactly" value={this.state.fields.exactly} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.exactly}</div>

            <label>Date of Drive:</label>
            <input type="date" name="when" value={this.state.fields.when} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.when}</div>

            {/* <label>Time of Drive:</label>
            <input type="time" name="time" value={this.state.fields.time} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.time}</div>

            <label>Last date to Apply:</label>
            <input type="date" name="before" value={this.state.fields.before} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.before}</div> */}

            <label>URL:</label>
            <input type="text" name="url" value={this.state.fields.url} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.url}</div>

            <label>Image:</label>
            <input type="text" name="image" value={this.state.fields.image} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.image}</div>

            <label>Eligibility:</label>
            <textarea className="description_area" type="text" name="eligibility" value={this.state.fields.eligibility} onChange={this.handleChange} rows={5} coloumns={100}></textarea>
            <div className="errorMsg">{this.state.errors.eligibility}</div>

            <label>More Information:</label>
            <textarea className="description_area" type="text" name="more" value={this.state.fields.more} onChange={this.handleChange} rows={5} coloumns={20}></textarea>
            <div className="errorMsg">{this.state.errors.more}</div>

            <label>Mobile No:</label>
            <input  type="text" name="phone" value={this.state.fields.phone} onChange={this.handleChange} />
            <div className="errorMsg">{this.state.errors.phone}</div>

            <input type="submit" className="button" value="Register"  onClick={this.submituserRegistrationForm} />
          </form>
        </div>
      </div>

    );
  }
}
function mapStateToProps(state) {
  
  return {
   create_drive_status : state.drivesReducer.is_drive_created,
   created_drive_response : state.drivesReducer.create_drive_response,
   userData: state.loginReducer.user_details
  }
}
function mapDispatchToProps(dispatch) {
  return {
    create_my_Drive : bindActionCreators(driveActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListHolder);