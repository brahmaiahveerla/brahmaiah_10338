import React, { Component } from 'react';

import '../../assets/css/ModuleStyles/ArticleStyles/searchBarStyle.css';

class SearchBar extends Component {
	constructor() {
		super();
		this.state = {
			query: ""
		}
	}

	handleInputChange = (e) => {
		this.setState({
			query: e.target.value
		})
	}

	componentDidUpdate() {
		console.log(this.state.query);
	}

	render() {
		return (
			<React.Fragment>
				<div className="searchbar-holder">
					<div className='searchbar-console flex-row'>
						<span><input type="text" placeholder="Search.." className="search-input-txt" name="search" onChange={this.handleInputChange}></input></span>
						<button type="submit"><i class="fa fa-search"></i></button>
					</div>
				</div>
			</React.Fragment>
				)
			}
		}
		
		export default SearchBar;
