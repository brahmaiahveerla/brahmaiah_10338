import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import * as authActions from '../../store/actions/roleAction';

class UserRole extends Component {
    constructor(props) {
        super(props);
        this.state={
            userData : {}
        }
    }
 
    render() {
      
        if (this.props.userData) {
            if (this.props.userData.response.role == "2") {
                return <Redirect to='/dashboard' />
            }
            else if (this.props.userData.response.role == "3") {
                if (window.confirm("you are logged in as Interviewer")) {
                    window.location.href = '/';
                }
            }
            else if (this.props.userData.response.role == 2) {
                if(window.confirm("you are logged in as Employer")){
                    window.location.href = '/';
                }
            }
        }
        return (
            <h1>Loading.....</h1>
        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    return {
        userData: state.loginReducer.user_details,
    };
}

function mapDispatchToProps(dispatch) {

    return {
        authActions: bindActionCreators(authActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(UserRole);
