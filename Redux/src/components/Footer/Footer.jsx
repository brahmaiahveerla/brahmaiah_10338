// npm dependencies
import React, { Component } from "react";
import { Link } from 'react-router-dom';


//styles/assets css file
// import '../../assets/css/LayoutStyles/headerStyles.css';
import '../../assets/css/LayoutStyles/footer.css';

/**
 * Header Component which is common to all other Components
 * (Component with YoA Logo)
 */
class Footer extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <React.Fragment>
        <div>
          <div className="footer">
            <div className="footer-content">
              <div className="site-mta-footer">
                <a className="footer-test" href="/aboutus/">About us</a>
                <a className="footer-test" href="/contactus/">Contact us</a>
                <a className="footer-test" href="/privacy/">Terms and Privacy Policy</a>
              </div>
              <p className="copy-right-msg">2019 WAY 2 WALKIN, Inc All Rights Reserved</p>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Footer;
