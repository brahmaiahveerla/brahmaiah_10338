
import React, { Component } from 'react'
import { Link } from "react-router-dom";

// component dependencies
import CreateArticle from './createDrive';

import SearchBar from '../SearchBar/searchBar';
import Navbar from '../Navbar/navbar';
// import RecruiterDriveCards from '../Cards/RecruiterDriveCards';
import EmployerDriveCards from '../Cards/EmployerDriveCards';


import { FilePond } from 'react-filepond';
import 'filepond/dist/filepond.min.css';


import { connect } from 'react-redux'

//Action imports
import * as driveActions from '../../store/actions/driveActionCreator';
import { bindActionCreators } from 'redux';
import ListHolder from '../ListHolder/listHolder';


//import service file
//importing modal
import DeleteArticleModal from '../Modals/deleteArticle.modal';


// styles/assets css file 
import '../../assets/css/BaseStyles/baseStyles.css';
import '../../assets/css/LayoutStyles/helperStyles.css';
import '../../assets/css/LayoutStyles/bodyStyles.css';
import '../../assets/css/ModuleStyles/ArticleStyles/searchBarStyle.css';
import '../../assets/css/ModuleStyles/ArticleStyles/leftMenuStyles.css';
import '../../assets/css/BaseStyles/navStyles.css';
import '../../assets/css/ModuleStyles/ArticleStyles/articleCardStyles.css';
import '../../assets/css/LayoutStyles/footer.css'

// // pagination dependency
// import Pagination from "react-js-pagination";
// import "bootstrap-less";

//creating the create article button
function CreateArticleBtn() {
  return (
    <React.Fragment>
      {/* <Link to="/recruiter/createarticle"><button className='create-article-btn'>Create Drive</button></Link> */}
      <Link to="/listholder"><button className='create-article-btn'>Create Drive</button></Link>
    </React.Fragment>
  )
}

/**
 * All Article is to display the total number of articles 
 * And These articles are being managed by the Admin
 */
class EmployerDrives extends Component {
  constructor(props) {
    super(props);
    this.state={
      selectedFile:null
    }
  }
  componentWillMount() {
    var userId=localStorage.getItem('userId')
    this.props.driveAction.fetchEmployerDrives(userId);
  }

  fileSelectHandler = event => {
    
    this.setState({selectedFile:event.target.files[0] })
      }

fileUpLoadHandler = () => {
  console.log(this.state.selectedFile);
  const filedata = new FormData();
  filedata.append('file',this.state.selectedFile,this.state.selectedFile.name)
  console.log(filedata);
} 
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <div className="article-container">
          <div className='allarticle-topbar flex-row'>
            <CreateArticleBtn />

            <SearchBar />
            <input type="file" onChange={this.fileSelectHandler} /> 
            <button onClick={this.fileUpLoadHandler}>upload</button>
            {/* <FilePond allowMultiple={true} server="www.googleapis.com"/> */}
           
          </div>
          <div className='allarticle-mainbody-holder flex-row'>
            <div className='allarticle-mainbody-rightsection'>
              <div className='list-holder'>
                <div>
                  <EmployerDriveCards />
                </div>
              </div>
            </div>
          </div>        
        </div>
        <div>
        </div>
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    Rec_DriveDetails: state.drivesReducer.employerDrives,
    userData: state.loginReducer.user_details
  }
}

function mapDispatchToProps(dispatch) {
  return {
    driveAction: bindActionCreators(driveActions, dispatch),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(EmployerDrives);