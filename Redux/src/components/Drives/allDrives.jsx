// npm dependencies
import React, { Component } from 'react'
import { Link } from "react-router-dom";

// component dependencies
import CreateArticle from './createDrive';

import SearchBar from '../SearchBar/searchBar';
import LeftMenu from '../Sidemenu/leftMenu';
import Navbar from '../Navbar/navbar';
import ArticleCard from '../Cards/RecruiterDriveCards';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux'

//Action imports
import * as articleActions from '../../store/actions/driveActionCreator';
import { bindActionCreators } from 'redux';


//import service file
//importing modal
import RecruiterDrivePage from './recruiterDrivePage';
import EmployerDrivePage from './employerDrivePage';
import DeleteArticleModal from '../Modals/deleteArticle.modal';


// styles/assets css file 
import '../../assets/css/BaseStyles/baseStyles.css';
import '../../assets/css/LayoutStyles/helperStyles.css';
import '../../assets/css/LayoutStyles/bodyStyles.css';
import '../../assets/css/ModuleStyles/ArticleStyles/searchBarStyle.css';
import '../../assets/css/ModuleStyles/ArticleStyles/leftMenuStyles.css';
import '../../assets/css/BaseStyles/navStyles.css';
import '../../assets/css/ModuleStyles/ArticleStyles/articleCardStyles.css';

// // pagination dependency
// import Pagination from "react-js-pagination";
// import "bootstrap-less";

//creating the create article button
// function CreateArticleBtn() {
//   return (
//     <React.Fragment>
//       <Link to="/admin/createarticle"><button className='create-article-btn'>Create Drive</button></Link>
//     </React.Fragment>
//   )
// }

/**
 * All Article is to display the total number of articles 
 * And These articles are being managed by the Admin
 */
class AllArticles extends Component {
  constructor(props) {
    super(props);
    this.state={
      userData:{}
    }
  }
  
  render() {
  
    
    let myRole = this.props.userData.response.role;
    if(myRole == "2"){
     return <Redirect to="/Edrives" />
    }
    else if(myRole == "4"){
      return <Redirect to="/Rdrives" />
    }
    else if(myRole = "3"){
     return  <Redirect to="/Idrives" />
    }
    else if(myRole = "1"){
      return  <Redirect to = "/Udrives" />
    }
    else if(myRole = "5"){
      return  <Redirect to = "./" />
    }
   
    return (
      <div>
        {
          (this.props.userData)?
          <div></div>:
          <div>Loading...!</div>
        }
      </div>
    )
  }
}

function mapStateToProps(state) {

  
  return {
    userData: state.loginReducer.user_details
  }
}

function mapDispatchToProps(dispatch) {
  return {
    articleActions: bindActionCreators(articleActions, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllArticles);