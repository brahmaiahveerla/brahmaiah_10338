import React, { Component } from 'react';

import { Link } from 'react-router-dom';


import * as driveActions from '../../store/actions/driveActionCreator';

import { bindActionCreators } from 'redux';

import { connect } from 'react-redux';
import EmployerDetails from '../Cards/EmployerDriveDetailsCards'


class EmployerDriveDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            candidatesOfDrive: [],
            driveDetails: [],
        }
    }
    componentWillMount() {
        this.props.driveActions.fetchDrivesDetails(this.props.match.params.id)
        this.props.driveActions.fetchDriveInfo(this.props.match.params.id)
    }

    render() {
        return (
            <div>
                {
                    (this.props.candidatesOfDrive)
                        ?
                        <div> 
                            {<EmployerDetails Details={this.props.candidatesOfDrive} />}

                        </div>
                        : <div>Loading..</div>
                }
            </div>

        )
    }

}
function mapStateToProps(state) {
    console.log(state)
    return {
        candidatesOfDrive: state.drivesReducer.driveDetails,
        driveDetails:state.drivesReducer.driveInfo,
    }

}
function mapDispatchToProps(dispatch) {
    return {
        driveActions: bindActionCreators(driveActions, dispatch),
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(EmployerDriveDetails);