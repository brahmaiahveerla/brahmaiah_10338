
import React, { Component } from 'react'
import { Link } from "react-router-dom";

import SearchBar from '../SearchBar/searchBar';
import Navbar from '../Navbar/navbar';
import ArticleCard from '../Cards/RecruiterDriveCards';
import UserDriveCards from '../Cards/UserDriveCards';

import { connect } from 'react-redux'

//Action imports
import * as articleActions from '../../store/actions/driveActionCreator';
import { bindActionCreators } from 'redux';

// styles/assets css file 
import '../../assets/css/BaseStyles/baseStyles.css';
import '../../assets/css/LayoutStyles/helperStyles.css';
import '../../assets/css/LayoutStyles/bodyStyles.css';  
import '../../assets/css/ModuleStyles/ArticleStyles/searchBarStyle.css';
import '../../assets/css/ModuleStyles/ArticleStyles/leftMenuStyles.css';
import '../../assets/css/BaseStyles/navStyles.css';
import '../../assets/css/ModuleStyles/ArticleStyles/articleCardStyles.css';

/**
 * All Article is to display the total number of articles 
 * And These articles are being managed by the Admin
 */
class UserDrives extends Component {
  constructor(props) {
    super(props);
    this.state ={
      myDriveDetails:{}
    }
  }
  componentWillMount() {
    let id = this.props.userData.response.userId;
    console.log(id);
    this.props.articleActions.fetchDrives(id);
  }
  
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <div className="article-container">
          <div className='allarticle-topbar flex-row'>
            <SearchBar />
          </div>
          <div className='allarticle-mainbody-holder flex-row'>
            <div className='allarticle-mainbody-rightsection'>
              <div className='list-holder'>
                <div>
                  <UserDriveCards />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div>
        </div>
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) { 
  return {
    myDriveDetails: state.drivesReducer.myDrives,
    userData: state.loginReducer.user_details
  }
}

function mapDispatchToProps(dispatch) {
  return {
    articleActions: bindActionCreators(articleActions, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserDrives);