
import React, { Component } from 'react'
import { Link } from "react-router-dom";

// component dependencies
import CreateArticle from './createDrive';

import SearchBar from '../SearchBar/searchBar';
import Navbar from '../Navbar/navbar';
import RecruiterDriveCards from '../Cards/RecruiterDriveCards';


import { connect } from 'react-redux'

//Action imports
import * as driveActions from '../../store/actions/driveActionCreator';
import { bindActionCreators } from 'redux';


//import service file
//importing modal
import DeleteArticleModal from '../Modals/deleteArticle.modal';


// styles/assets css file 
import '../../assets/css/BaseStyles/baseStyles.css';
import '../../assets/css/LayoutStyles/helperStyles.css';
import '../../assets/css/LayoutStyles/bodyStyles.css';
import '../../assets/css/ModuleStyles/ArticleStyles/searchBarStyle.css';
import '../../assets/css/ModuleStyles/ArticleStyles/leftMenuStyles.css';
import '../../assets/css/BaseStyles/navStyles.css';
import '../../assets/css/ModuleStyles/ArticleStyles/articleCardStyles.css';

// // pagination dependency
// import Pagination from "react-js-pagination";
// import "bootstrap-less";

//creating the create article button
function CreateArticleBtn() {
  return (
    <React.Fragment>
      <Link to="/recruiter/createarticle"><button className='create-article-btn'>Create Drive</button></Link>
    </React.Fragment>
  )
}

/**
 * All Article is to display the total number of articles 
 * And These articles are being managed by the Admin
 */
class RecruiterDrives extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    let id = this.props.userData.response.userId;
    this.props.driveAction.fetchRecruiterDrives(id);
  }
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <div className="article-container">
          <div className='allarticle-topbar flex-row'>
            <CreateArticleBtn />
            <SearchBar />
          </div>
          <div className='allarticle-mainbody-holder flex-row'>
            <div className='allarticle-mainbody-rightsection'>
              <div className='list-holder'>
                <div>
                  <RecruiterDriveCards />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div>
        </div>
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    Rec_DriveDetails: state.drivesReducer.recruiterDrives,
    userData: state.loginReducer.user_details
  }
}

function mapDispatchToProps(dispatch) {
  return {
    driveAction: bindActionCreators(driveActions, dispatch),
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(RecruiterDrives);