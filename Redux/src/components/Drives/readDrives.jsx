import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as allAction from '../../store/actions/driveActionCreator';
import * as allDriveActions from '../../store/actions/driveRegistrationActionCreators';
import '../../assets/css/ModuleStyles/ArticleStyles/readarticle.css';
import { Link } from 'react-router-dom';


class ReadDrive extends Component {
  constructor(props) {
    super(props);
    this.state = {

    }
    this.registering = this.registering.bind(this);
  }

  componentWillMount() {
    this.props.allActions.fetchDrivesById(this.props.match.params.id)
  }

  registering = () => {

    
    this.props.driveAction.driveRegistration({
      driveId: this.props.match.params.id,
      userId: this.props.userData.response.userId
    })
  }
  render() {

    return (
      <div>
        {
          (this.props.single_Drive_Status) ?
            (this.props.singleDrive) ?
              <div className="container">
                <div className="row">
                  <h3 id="main">Drive details</h3>
                  <p id="go_back"><Link to="/article"> All Drives</Link></p>
                </div><br /><hr />
                <h5>{this.props.singleDrive.heading}</h5>
                <p>Description:{this.props.singleDrive.time}</p>
                <p>Venue:{this.props.singleDrive.where}</p><br />
                <h6 id="article_brief">Date and time : {this.props.singleDrive.when}</h6>
                <img className="drive-image" src="https://www.gcreddy.com/wp-content/uploads/2018/12/Software-Testing-Jobs-7th-December.jpg" />
                <br></br>
                <p>Eligibility:{this.props.singleDrive.eligibility}</p>
                <br></br>
                <p>More Information : {this.props.singleDrive.more} </p>
                {/* {
                  (this.props.userData)?
                  (this.props.userData.response.role == "1")?
                  <div>
                    <UserDriveFooter/>
                  </div>:
                  <div></div>:
                  <div>Loading...</div>
                } */}
                 {/* {
                  (this.props.userData)?
                  (this.props.userData.response.role == "4")?
                  <div>
                    <RecruiterDriveFooter/>
                  </div>:
                  <div></div>:
                  <div>Loading...</div>
                } */}
                 {
                  (this.props.userData)?
                  (this.props.userData.response.role == "3"  )?
                  <div>
                    {/* <InterviewerDriveFooter/> */}
                    <p>welcome to the Interviewer.</p>
                  </div>:
                  <div></div>:
                  <div>Loading...</div>
                }
                {
                  (this.props.userData)?
                  (this.props.userData.response.role == "2"  )?
                  <div>
                    {/* <EmployerDriveFooter/> */}
                    <p>welcome to the Interviewer.</p>
                  </div>:
                  <div></div>:
                  <div>Loading...</div>
                }
              </div> :
              <div>Loading..</div> :
            <div>Loading..</div>
        }
      </div>
    );
  }
}
function mapStateToProps(state) {
  
  return {
    singleDrive: state.drivesReducer.singleDrive,
    single_Drive_Status: state.drivesReducer.single_drive_status,
    userData : state.loginReducer.user_details
  }
}
function mapDispatchToProps(dispatch) {
  return {
    allActions: bindActionCreators(allAction,dispatch),
    driveAction : bindActionCreators( allDriveActions,dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ReadDrive);