import request from 'superagent'

import * as driveActions from '../actions/driveActionCreator';
import * as allActions from '../actions/action.constants';
import * as driveRegistrationActions from '../actions/driveRegistrationActionCreators';

const articlesService = (store) => next => action => {

    next(action)
    switch (action.type) {
        case allActions.FETCH_DRIVES:
            request.get('http://192.168.150.70:8080/api/drives/candidate' + `/${action.payload}`)
                .send(action.payload)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .set('content-type', 'application/json')
                .then(res => {
                    const data = JSON.parse(res.text);
                    if (data.statusCode == 1) {
                        next(driveActions.receiveDrives(data));
                    }
                })
                .catch(err => {
                    next({ type: "FETCH_ARTICLES_DATA_ERROR", err });

                });

            break;

        case allActions.FETCH_RECRUITER_DRIVE:
            request.get('http://192.168.150.70:8080/api/recruiter/drives' + `/${action.payload}`)
                .send(action.payload)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .set('content-type', 'application/json')
                .then(res => {
                    const data = JSON.parse(res.text);
                    if (data.statusCode == 1) {
                        next(driveActions.receiveRecruiterDrives(data));
                    }
                })
                .catch(err => {
                    next({ type: "FETCH_ARTICLES_DATA_ERROR", err });

                });

            break;

        case allActions.FETCH_EMPLOYER_DRIVE:
            request.get('http://192.168.150.70:8080/api/drives/employer' + `/${action.payload}`)
                .send(action.payload)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .set('content-type', 'application/json')
                .then(res => {
                    const data = JSON.parse(res.text);
                    if (data.statusCode == 1) {
                        next(driveActions.receiveEmployerDrives(data));
                    }
                })
                .catch(err => {
                    next({ type: "FETCH_ARTICLES_DATA_ERROR", err });

                });

            break;

        case allActions.FETCH_DRIVE_DETAILS_BY_ID:
            request.get('http://192.168.150.70:8080/api/employer/getInterviews/' + `${action.payload}`)
                .send(action.payload)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .set('content-type', 'application/json')
                .then(res => {
                    const data = JSON.parse(res.text);
                    if (data.statusCode == 1) {
                        next(driveActions.receiveDrivesDetails(data));
                    }
                })
                .catch(err => {
                    next({ type: "FETCH_DRIVE_DATA_ERROR", err });

                });

            break;
            

        case allActions.CREATE_DRIVE:
            request.post('http://192.168.150.70:8080/api/create/drives/' + `${action.payload.id}`, action.payload.field)
                .send(action.payload)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .set('Content-Type', 'application/json')
                .then(res => {
                    const data = JSON.parse(res.text);
                    if (data.statuscode == 1) {
                        next(driveActions.createDriveStatus(data))
                    }
                })
                .catch(err => {
                    next({
                        type: "posting tha New article Error",
                        err
                    })
                })

            break;

        case allActions.FETCH_DRIVE_BY_ID:
            request.get('http://192.168.150.70:8080/api/drives' + `/${action.payload.id}` + `/${action.payload.userId}`)
                .send(action.payload)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .set('Content-Type', 'application/json')
                .then(res => {
                    const data = JSON.parse(res.text);
                    if (data.statusCode == 1) {
                        next(driveActions.receiveDrivesById(data))
                    }
                })
                .catch(err => {
                    next({ type: "FETCH_ARTICLES__BY_ID_DATA_ERROR", err });

                });

            break;

        case allActions.DRIVE_REGISTRATION:

            request.get('http://192.168.150.70:8080/api/drives' + `/${action.payload.driveId}` + '/register' + `/${action.payload.userId}`)
                .set('content-type', 'application/json')
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .then(res => {

                    const data = JSON.parse(res.text);
                    if (data.statuscode == 1) {
                        next(driveRegistrationActions.driveRegistrationStatus(data));
                    }
                })
            break;

        case allActions.FETCH_DRIVE_INFO:
            var userId = localStorage.getItem('userId')
            request.get('http://192.168.150.70:8080/api/drives' + `/${action.payload}` + `/${userId}`)

                .set('Content-Type', 'application/json')
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .then(res => {
                    const data = JSON.parse(res.text);
                    if (data.statusCode == 1) {
                        next(driveActions.receiveDriveInfo(data))
                    }
                })
                .catch(err => {
                    next({ type: "FETCH_DRIVE__INFO_DATA_ERROR", err });

                });

            break;

        default:
            break;
    };
};

export default articlesService;