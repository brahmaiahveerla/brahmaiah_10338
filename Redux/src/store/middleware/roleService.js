import request from 'superagent';
import * as roleActions from '../actions/roleAction';
import * as allActions from '../actions/action.constants';

const roleService = (store) => next => action => {
    next(action)
    
        switch (action.type) {
   
        case allActions.FETCH_USER:
            request.get('http://13.250.235.137:8050/api/user/')
                .send(action.payload)
                .set('Authorization', (localStorage.getItem('jwt-token')))
                .set('Content-Type', 'application/x-www-form-urlencoded')
              
                .then(res => {
                    const data = JSON.parse(res.text);
                
                    next(roleActions.doRecieveUser(data));

                })
                .catch(err => {
                    next({ type: 'FETCH_ROLE_DATA_ERROR', err });
                });
            break;

        default: break;
    };
};

export default roleService;