import request from 'superagent'
import * as allActions from '../actions/action.constants'
import * as regisrationActions from '../actions/newRegistrationCreator';

const registrationService = store => next => action =>{
    next(action)
    switch(action.type){
        case allActions.NEW_USER_REGISTRATION:
        request.post('http://192.168.150.70:8080/api/register',action.payload)
        .send(action.payload)
        .set('Authorization', (localStorage.getItem('jwt-token')))
        .set('Content-Type','application/json')
        .then(res =>{
            const data = JSON.parse(res.text);
            if(res.statusCode == 1){
                next(regisrationActions.registrationStatus(data));
            }
            else{
                next({
                    type: 'REGISTRATION_FAILURE',
                    payload: {}
                })
            }
        })
        break;

        default:
        break;
    }
}
export default registrationService;