import request from 'superagent'
import * as allActions from '../actions/action.constants'
import * as authAction from '../actions/authActionCreator';

const authService = store => next => action => {
    next(action)
   
    switch(action.type){
        case allActions.DO_LOGIN_USER:
            request.post('http://192.168.150.70:8080/api/login',action.payload)
            .send(action.payload) 
            .set('Content-Type','application/json')
            .then(res => {
                const data = JSON.parse(res.text);
                if(data.statusCode == 1){
                    localStorage.setItem('userId',data.response.userId);
                    localStorage.setItem('jwt-token',data.token);
                    localStorage.setItem('user-Role',data.response.role);

                    next(authAction.loginUserSucess(data));
                }
                else{
                    next({
                        type: allActions.LOGIN_USER_DATA_ERROR,
                            payload: data.statuscode
                    });
                }
            })
            .catch(err => {
                return next({ type: 'LOGIN_USER_DATA_ERROR',err })
            });

            break;

            default: break;
    };
};

export default authService;