import * as allActions from '../actions/action.constants'

const initialState = {
    is_logged_in : false,
    user_details : {},
}

export default function authReducer(state = initialState,action){
   
    switch(action.type){
        case allActions.LOGIN_USER_SUCCESS:
            return{
                ...state,
                is_logged_in : true,
                user_details : action.payload
            }
        case allActions.LOGIN_USER_DATA_ERROR:
        return {
            ...state,
         is_logged_in : false,
         statuscode:action.payload
        }
        default:
            return state;
    }
}