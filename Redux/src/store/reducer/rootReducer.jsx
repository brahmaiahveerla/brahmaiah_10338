import { combineReducers } from 'redux'

import drivesReducer from './drivesReducer';
import loginReducer from './authReducer';
import bulletinsReducer from './bulletinReducer';
import roleReducer from './roleReducer';
import registrationReducer from './registrationReducer';

const rootReducer = combineReducers({
    loginReducer,
    drivesReducer, 
     registrationReducer
});


export default rootReducer;