import * as allActions from '../actions/action.constants';

const initialState = {
    registrationResponse:{},
    registration_status : false
}

export default function newRegistrationReducer(state = initialState,action){
    switch(action.type){
        case allActions.REGISTRATION_STATUS:
 
        return{
            ...state,
            registration_status: true,
            registrationResponse: action.payload
        }

        default:
        return state;
    }
}