import * as allActions from '../actions/action.constants'

const initialState = {
  myDrives: {},
  isLoaded: false,
  singleDrive: {},
  single_drive_status: false,
  recruiterDrives: {},
  is_R_Loaded: false,
  create_drive_response: {},
  is_drive_created: false,
  employerDrives:[],
  is_E_Loaded:false,
  driveDetails:[],
  driveInfo:[]
}


export default function articlesReducer(state = initialState, action) {

  switch (action.type) {

    case allActions.FETCH_DRIVES:
      return action;

    case allActions.RECEIVE_DRIVES:
      return {
        ...state,
        myDrives: action.payload.response,
        isLoaded: true
      }
    case allActions.RECEIVE_DRIVE_BY_ID: 
      return {
        ...state,
        singleDrive: action.payload.response,
        single_drive_status: true
      }

    case allActions.RECEIVE_RECRUITER_DRIVE:
      return {
        ...state,
        recruiterDrives: action.payload.response,
        is_R_Loaded: true
      }

      case allActions.RECEIVE_EMPLOYER_DRIVE:
      
        return{
          ...state,
          employerDrives: action.payload.response,
          is_E_Loaded: true
        }

    case allActions.CREATE_DRIVE_STATUS:
      return {
        ...state,
        create_drive_response: action.payload,
        is_drive_created: true
      } 
    case allActions.DRIVE_REGISTER_STATUS:
      return {
        ...state,
        driveRegisterStatus: action.payload,
        is_register: true
      }

      case allActions.FETCH_DRIVE_DETAILS_BY_ID:
      return action;

      case allActions.RECEIVE_DRIVE_DETAILS_BY_ID:
      return{
        ...state,
        driveDetails:action.payload.response 
      }



      case allActions.FETCH_DRIVE_INFO:
      return action;

      case allActions.RECEIVE_DRIVE_INFO:
      return{
        ...state,
        driveInfo :action.payload.response 
      }


    default:
      return state;
  }
}