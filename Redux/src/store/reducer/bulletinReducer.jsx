import * as allActions from '../actions/action.constants'

const initialState = {
    bulletins    : [],
    isLoaded     : false,
    Items        : {}
}

export default function bulletinsReducer(state = initialState,action){
    switch (action.type){
        case allActions.FETCH_BULLETINS:
            return action;

        case allActions.RECEIVE_BULLETINS:

            return{
                ...state,
               bulletins : action.payload.bulletins,
                isLoaded : true
            };
        default:
            return state;
    }
}