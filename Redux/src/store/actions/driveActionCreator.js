import * as allActions from './action.constants'

export function receiveDrives(data) {
   
    return {
        type: allActions.RECEIVE_DRIVES,
        payload: data
    };
}

export function fetchDrives(id) {
    return {
        type: allActions.FETCH_DRIVES,
        payload: id
    };
}

export function receiveDrivesById(data){
    console.log(data);
    return { 
        type : allActions.RECEIVE_DRIVE_BY_ID, 
        payload : data 
    };
}

export function fetchDrivesById(id){
   return {
        type : allActions.FETCH_DRIVE_BY_ID,
        payload : {id}
     };
}

export function receiveRecruiterDrives(data) {
   
    return {
        type: allActions.RECEIVE_RECRUITER_DRIVE,
        payload: data
    };
}

export function fetchRecruiterDrives(id) {
    return {
        type: allActions.FETCH_RECRUITER_DRIVE,
        payload: id
    };
}

export function receiveEmployerDrives(data){
    return{
        type: allActions.RECEIVE_EMPLOYER_DRIVE,
        payload: data
    };
}

export function fetchEmployerDrives(id){
    return{
        type: allActions.FETCH_EMPLOYER_DRIVE,
        payload: id
    };
}

export function createDrive(data) {
    return {
        type: allActions.CREATE_DRIVE,
        payload: data
    }
}
export function createDriveStatus(data) {
    return {
        type: allActions.CREATE_DRIVE_STATUS,
        payload: data
    }
}



export function fetchDrivesDetails(id){
    return{
        type:allActions.FETCH_DRIVE_DETAILS_BY_ID,
        payload:id
    };
}

export function receiveDrivesDetails(data){
    return{
        type:allActions.RECEIVE_DRIVE_DETAILS_BY_ID,
        payload:data
    }
}

export function fetchDriveInfo(id){
     console.log(id)

    return{
        type:allActions.FETCH_DRIVE_INFO,
        payload:id
    };
}

export function receiveDriveInfo(data){
    return{
        type:allActions.RECEIVE_DRIVE_INFO,
        payload:data
    }
}


 
 

 
 