import * as allActions from './action.constants';

export function newUserRegistration(data){
    return{
        type: allActions.NEW_USER_REGISTRATION,
        payload: data
    }
}
export function registrationStatus(data){
    return{
        type: allActions.REGISTRATION_STATUS,
        payload:data
    }
}