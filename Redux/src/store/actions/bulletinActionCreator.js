import * as allActions from './action.constants'

export function receiveBulletins(data){
   
    console.log(data);
    return { type : allActions.RECEIVE_BULLETINS, payload : data };
}

export function fetchBulletins(){
   
   return { type : allActions.FETCH_BULLETINS,payload : {} };
}
