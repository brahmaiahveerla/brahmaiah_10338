// Action constants for auth scenario
export const DO_LOGIN_ADMIN = 'DO_LOGIN_ADMIN';
export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';

// Actions constants for bulletin scenario  
export const DELETE_BULLETIN = 'DELETE_BULLETIN';

// Actions constants for user scenario  
export const DELETE_USER = 'DELETE_USER';

// Actions constants for complain scenario  
export const RESOLVE_USER = 'RESOLVE_USER';

// Action constants for drive scenario
export const CREATE_DRIVE = 'CREATE_DRIVE';
export const CREATE_DRIVE_STATUS = 'CREATE_DRIVE_STATUS'

export const EDIT_ARTICLE = 'EDIT_ARTICLE';

/*
 *Action constatns for Login service 
 */
export const DO_LOGIN_USER = 'DO_LOGIN_USER'
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCESS'

export const LOGIN_USER_DATA_ERROR='LOGIN_USER_DATA_ERROR';

/**
 * Logout Service
 */
export const LOGOUT_USER = 'LOGOUT_UER'

/*
*Actions constants for viewArticleService
*/
export const FETCH_DRIVES = 'FETCH_DRIVES'
export const RECEIVE_DRIVES = 'RECEIVE_DRIVES'

/*
*Actions constants for Role
*/

export const FETCH_USER='FETCH_USER';
export const RECIEVE_USER='RECIEVE_USER';

/**
 * Actions constants for Updating Articles
 */
export const UPDATE_ARTICLES = 'UPDATE_ARTICLES'
export const RECEIVE_UPDATE_ARTICLES = 'RECEIVE_UPDATE_ARTICLES'

/*
*Actions constants for viewBulletinService
*/
export const FETCH_BULLETINS = 'FETCH_BULLETINS'
export const RECEIVE_BULLETINS = 'RECEIVE_BULLETINS'

export const FETCH_DRIVE_BY_ID = 'FETCH_DRIVE_BY_ID '
export const RECEIVE_DRIVE_BY_ID = 'RECEIVE_DRIVE_BY_ID'

/*
*Actions constants for delete the article
*/
export const DELETE_ARTICLE = 'DELETE_ARTICLE'
export const DELETE_ARTICLE_STATUS=' DELETE_ARTICLE_STATUS'
/**
 * Acrtion constants for Registration of New Users.
 */
export const NEW_USER_REGISTRATION = 'NEW_USER_REGISTRATION';
export const REGISTRATION_STATUS = 'REGISTRATION_STATUS';
/**
 * Drive Registration constants
 */
export const DRIVE_REGISTRATION = 'DRIVE_REGISTRATION'
export const DRIVE_REGISTER_STATUS = 'DRIVE_REGISTER_STATUS'

/**
 * Drive Constants for Recruiter senario
 */
export const FETCH_RECRUITER_DRIVE ='FETCH_RECRUITER_DRIVE'
export const RECEIVE_RECRUITER_DRIVE = 'RECEIVE_RECRUITER_DRIVE'

export const FETCH_EMPLOYER_DRIVE = 'FETCH_EMPLOYER_DRIVE'
export const RECEIVE_EMPLOYER_DRIVE = 'RECEIVE_EMPLOYER_DRIVE'


export const FETCH_DRIVE_DETAILS_BY_ID = 'FETCH_DRIVE_DETAILS_BY_ID'
export const RECEIVE_DRIVE_DETAILS_BY_ID = 'RECEIVE_DRIVE_DETAILS_BY_ID'


export const FETCH_DRIVE_INFO = 'FETCH_DRIVE_INFO'
export const RECEIVE_DRIVE_INFO = 'RECEIVE_DRIVE_INFO'


 


