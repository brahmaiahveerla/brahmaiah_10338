import * as allActions from './action.constants'

export function driveRegistration(data){
    return{
        type: allActions.DRIVE_REGISTRATION,
        payload:data
    }
}
export function driveRegistrationStatus(data){
    return{
        type:allActions.DRIVE_REGISTER_STATUS,
        payload: data
    }
}