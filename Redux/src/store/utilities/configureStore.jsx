import { createStore,applyMiddleware,compose } from  'redux'
import rootReducer from '../reducer/rootReducer';

import authMiddleware from '../middleware/authService'

import articlesMiddleware from '../middleware/allDrivesService';

import bulletinsMiddleware from '../middleware/allBulletinService';

import roleMiddleware from '../middleware/roleService';

import allDrivesMiddleware from '../middleware/allDrivesService';

import registrationMiddleware from '../middleware/regisrationService';

export default function configureStore(){
    return createStore(
        rootReducer,
        compose(applyMiddleware(
            authMiddleware,
            articlesMiddleware,
            bulletinsMiddleware,
            registrationMiddleware,
            allDrivesMiddleware,
        ))
    );
};